#! user/bin/bash
# read x
# read y
read a
read b
read c
# -lt: less than
# -gt: greater than
# -eq: equal
# -le: less than or equal
# -ge: greater than or equal
# -a: and
# -o: or

if [ $a -eq $b -a $b -eq $c -a $a -eq $c ]
then
    echo EQUILATERAL
elif [ $a -eq $b -o $b -eq $c -o $a -eq $c ]
then
    echo ISOSCELES
else
    echo SCALENE
fi
# if [ $x -gt $y ]
# then
#     echo X is greater than Y
# elif [ $x -lt $y ]
# then
#     echo X es less than y
# elif [ $x -eq $y ]
# then
#     echo X es equal than y
# fi

